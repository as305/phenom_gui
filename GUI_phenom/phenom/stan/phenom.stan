data {
  int<lower=1> N; // number of time sampled
  int<lower=1> P; // number of replicates
  int<lower=1> K; // number of latent functions/coefficients
  int<lower=1> L; // number of unique priors
  int<lower=1, upper=L> prior[K]; // prior assignment for each function/coefficients
  real alpha_prior[L,2]; // sigma^2 in rbf kernel
  real lengthscale_prior[L,2]; // l parameter in rbf kernel
  real sigma_prior[2]; // standard deviation from the mean
  real ls_min;
  real ls_max;

  matrix[P,K] design; // coefficients
  row_vector[N] y[P]; // OD measurement
  real x[N];  // time
}
parameters {
  real<lower=ls_min, upper=ls_max> lengthscale[L];
  real<lower=0> alpha[L];
  real<lower=0> sigma;
  vector[N] f_eta[K];
}
transformed parameters {
  matrix[K,N] f;

  for (l in 1:L) // for each prior
  {
    matrix[N, N] L_cov;
    matrix[N, N] cov;
    cov = cov_exp_quad(x, alpha[l], lengthscale[l]);
    for (n in 1:N) // for each of the time point
      cov[n, n] = cov[n, n] + 1e-12;
    L_cov = cholesky_decompose(cov);

    for (k in 1:K) // for each coefficient
      {
        if (prior[k] == l) // if that coefficient use prior #l
          f[k] = (L_cov * f_eta[k])';
      }
  }
}
model {

  for (l in 1:L) // for each of the priors
  {
    lengthscale[l] ~ inv_gamma(lengthscale_prior[l,1], lengthscale_prior[l,2]);
    alpha[l] ~ gamma(alpha_prior[l,1], alpha_prior[l,2]);
  }

  sigma ~ gamma(sigma_prior[1], sigma_prior[2]);

  for (i in 1:K)
    f_eta[i] ~ normal(0, 1);

  for (i in 1:P)
    y[i] ~ normal(design[i]*f, sigma);
}
